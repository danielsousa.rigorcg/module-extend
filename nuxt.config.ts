// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: ['@vueuse/nuxt'],
  app: {
    head: {
      link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    },
  },
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `@import "~/assets/sass/abstracts";`,
        },
      },
    },
  },
  css: ['@/assets/sass/base/index.scss'],
  imports: {
    dirs: ['composables/**'],
  },
  components: {
    global: true,
    dirs: [
      '~/components/Atoms',
      '~/components/Molecules',
      '~/components/Organisms',
    ],
  },
});
